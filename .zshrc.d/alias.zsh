# Pacman
alias pacman="sudo pacman"
alias pacinstall="pacman -S"
alias pacupdate="sudo su; pacman -Syu; sudo reboot"
alias pacrm="pacman -Rs"
alias pacrms="pacman -R"
# Yaourt
# alias yaourt="sudo yaourt"
alias yaoinstall="yaourt -S"
alias yaoupdate="yaourt -Syu"
# Misc
alias tmux="tmux -2"
alias nvimrc="nvim ~/.config/nvim/init.vim"
alias nvimrcd="nvim ~/.config/nvim/"
alias zshrc="nvim ~/.zshrc"
alias zshrcd="nvim ~/.zshrc.d"
# Neovim
alias makefi="nvim Makefile"
# ls
alias ls="ls --color=auto"
alias l="ls -A --color=auto"
alias L="ls -lA --color=auto"
