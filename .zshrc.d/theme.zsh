PLUSMINUS="\u00b1"
BRANCH="\ue0a0"
DETACHED="\u27a6"
CROSS="\u2718"
LIGHTNING="\u26a1"
GEAR="\u2699"
prompt_text() {
	echo -n '%F{'$2'}['$1']%f'
}
prompt_var() {
	USER="%n"
	HOST="%m"
	FULLHOST="%M"
}
prompt_name() {
	if [[ -n "$SSH_CLIENT" ]]; then
		prompt_text  "$USER@$HOST" orange
	else 
		prompt_text "$USER" '%(!.green.yellow)'
	fi
}
prompt_location() {
	prompt_text "%(!.%3d.%3~)" magenta
}
prompt_end() {
	if [[ -d .git ]]; then
		echo -n "\n%(0?.±.%F{red}±%f) " 
	else
		echo -n "\n%(0?.%#.%F{red}%#%f) " 
	fi
}
left_prompt() {
	prompt_var
	prompt_name
	prompt_location
	_git_branch
	prompt_end
}

_git_branch() {
	local ref color
	if [[ -d .git ]]; then
		 ref=$(git symbolic-ref HEAD | cut -d'/' -f3)
		 is_dirty() {
			 test -n "$(git status --porcelain --ignore-submodules)" 
		 }
		 if is_dirty; then
			 color='red'
		 else 
			 color='green'
		 fi
		 prompt_text $ref $color
	fi
}
right_prompt() {
	prompt_text '%*' blue
}


PROMPT='%{%f%b%k%}$(left_prompt)'
RPROMPT='%{%f%b%k%}$(right_prompt)'
