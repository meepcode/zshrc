
# Lines configured by zsh-newuser-install
HISTFILE=~/.zshrc.d/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd beep prompt_subst
unsetopt appendhistory extendedglob nomatch notify
bindkey -v
# End of lines configured by zsh-newuser-install

source ~/.zshrc.d/var.zsh
source ~/.zshrc.d/completion.zsh
source ~/.zshrc.d/theme.zsh
source ~/.zshrc.d/alias.zsh
source ~/.zshrc.d/opt.zsh
